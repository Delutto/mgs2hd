﻿--demo2_english_0001.txt
What do you mean?
That's not your business.
Oh really?
It so happens I have some business|of my own to attend to.
Planning to hijack Arsenal?
!
You were going to screw|me over, weren't you?
...Who talked? Ocelot?
Not exactly.
I was the one who used Ocelot 
to suggest the idea to you |in the first place.
What?
I was planning to give you|Arsenal to begin with.
...Why the uncharacteristic generosity?
Hm. I'm no philanthropist.
Arsenal is far from impregnable.
It needs other Metal Gears as guards, |a huge payload of warheads,
and full air, sea and land support|to function efficiently.
Against a large attack|force without support,
Arsenal is nothing more|than a gigantic coffin.
Seizing Arsenal Gear was|never the real objective.
...What was your objective, then?
A list of names --
of the Patriots!
!?

--demo2_english_0002.txt
No -- there is another way.
Really...
But we have our own|plans to carry out.
We'll take the Arsenal since|you don't care for it anyway;
the purified hydrogen bomb|is ready to go.
A nuclear strike won't stop them.
It will damage their power source --
the mindless masses that they control.
First things first.
Of course --
that was what you wanted.
I won't stop you.
Good luck.
Thanks, but I have quite enough of that.
What exactly do you find so funny?
Charades usually are humorous.
I wouldn't have minded watching |some more of it,
but we're running a|little short on time...
What are you talking about?
Everything you've done here |has been scripted -- 
a little exercise set up by us.
Exercise!?
The S3 Plan was conceived as|a means to produce soldiers 
on par with Solid Snake.
That's what I told you.
But the VR training the boy was put |through is not the meat of the project.
You think this little terrorist incident|is your own doing, Solidus?
THIS is the S3 training kernel --
an orchestrated recreation|of Shadow Moses.
What!?

--demo2_english_0003.txt
Solidus,
you and the boy were selected|because your relationship
resembles the one between|Snake and Big Boss.
Fortune,
you and the rest of Dead Cell stand in|for the FOXHOUND squad
that Snake took on in Shadow Moses.
You're the most impressive collection|of freaks outside of FOXHOUND.
We've gone to a lot of trouble|to set you up against the boy.
That story about purified hydrogen |bombs is just the tip of the iceberg.
The project was already underway |when I sunk that tanker
along with your old man two years ago.
Throwing your husband in|the brig was a part of it too.
You were told that the eradication|of Dead Cell six months ago
was an act of the Patriots.
We provoked and encouraged |your hatred --
and you opted for vengeance,
just as we planned.
All orchestrated...?
Except for the appearance|of the real Solid Snake.
I wonder now, who sent for you...?
All our misfortune was --
just a part of their project!
H-how could -- !
You're no Lady Luck.
You have nothing that |we didn't give you.
What?

--demo2_english_0004.txt
Hm? Thought I got her in the heart?
It missed...
Now I remember.
Your heart's on the right.
Waste of metal, my dear.|Your luck's run out.
This is the little gizmo.
There's no such thing as miracles|or the supernatural#－
only cutting-edge technology.
HRR!!
You bastard...
Now that I have enough data,
all I have to do is retrieve Arsenal...
...and clean up the refuse|from the exercise.
Just try!
!?
How's this then!?
Damn!
Fortune!
You idiot! Get the hell|away from there!
I told you -- your luck's run out.
Take your reward:
it's all the payload RAY has.
Die!!
Everybody down!!!
What the -- ! Impossible!
She is Lady Luck.
My name is Helena Dolph Jackson.
The daughter of a proud,|noble soldier...
I can...
see my family...
again...
Damn!!
Try this instead!
No!
Hm?
No! 
No, not now!!
Brothers!!
Liquid!
I've been waiting for this.
It can't be -- ?

--demo2_english_0005.txt
I'm off to bury the Patriots for good.
You know where they are? How?
Why do you think I chose|Ocelot as my host?
But before I go, I have a family matter|to settle with both of you.
There's room for only one Snake, |and one Big Boss!
Grraah!
Time to say goodbye.
Damn!
Like surfing? It's a good way to go.
Liquid! Stop this thing!
Hey, Snake! You coming?
Hrrr-eya!!!
Snaaaake!!

--demo2_english_0006.txt
Federal Hall...
What are you laughing at?
Do you know what day it is today?
...April 30th?

--demo2_english_0007.txt
All you want is power -- at any cost.
Jack,
it's not power I want.
What I wanted to take back from |the Patriots were things like --
freedom, civil rights, opportunities.
The founding principles of this country.
Everything that's about to be wiped|out by their digital censorship.
Jack,
listen to me.
We're all born with an expiration date.
No one lasts forever.
Life is nothing but a grace period --
for turning the best of our genetic |material into the next generation.

--demo2_english_0008.txt
All I want is to be remembered. |By other people, by history.
The Patriots are trying to protect|their power, their own interests,
by controlling the digital flow of|information.
I want my memory, my existence |to remain.
Unlike an intron of history.
I will be remembered as an exon.
That will be my legacy,|my mark in history.
But the Patriots would deny us |even that.
I will triumph over the Patriots
and liberate us all.
And we will become#－
the "Sons of Liberty"!

--demo2_english_0009.txt
Jack...
my son.
My clone brothers and I are called |monsters -- replicates of evil genes...
You are one-of-a-kind --
But still a monster, shaped by|a dark and secret history.
We need to decide which monstrosity |will have the privilege of survival.
By the way, Jack,
I was the one who killed your parents.
!!

--demo2_english_0010.txt
It's time we were both free.
I have other reasons for|wanting you dead.
The clues to the Patriots inside|GW have been erased,
but there are other traces.
Inside YOU.
What?
The information is being carried by the|nanomachines in your cerebral cortex,
and throughout the neural|network they formed.
Brace yourself!!

--demo2_english_0011.txt
Who am I really...
No one quite knows|who or what they are.
The memories you have
and the role you were assigned|are burdens you had to carry.
It doesn't matter if they were real|or not. That's never the point.

--demo2_english_0012.txt
By the way, what is that?
Dog tags?
Anyone you know?
No, never heard the name before.
I'll pick my own name...
...and my own life.
I'll find something worth passing on.
They taught me some good things too.
I know. 
We've inherited freedom from|all those who've fought for it.
We all have the freedom|to spread the word. 
Even me.
Snake, what about Olga's child?
Don't worry. I'll find him. Count on it.
As long as you keep yourself|alive, he's safe.
Do you know where Liquid went?
I put a transmitter on his RAY.
Did he head for the Patriots?
Yeah. 
But I have a feeling they gave Ocelot|a bogus location to begin with.
...
Cheer up. We have a better lead.
This contains the list of all the Patriots.
But Ocelot took it!
The one we gave you|wasn't the real thing.
What?
This virus is coded to destroy|only a specific part of GW -- 
namely the information about|the Patriots' identity.
Which means 
that there's a parameter coded in here|that defines what that information is.
I get it -- 
analyze the code and you can probably|find out where they operate.
Count me in -- 
No, you have things to do first.
And people you need to talk to...
?
Snake?
What's wrong?
Nothing...
Can I ask you something?
Who am I really?
I wouldn't know.
...
But we're going to find out|together, aren't we?
Oh... 
Yeah...
See me for what I am, ok?
I know.
